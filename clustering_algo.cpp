#include <iostream>
#include <vector>
#include <string>
#include <fstream>


int main() {

    
    // setup variables
    int NCHANNELS = 25;
    // Work with x and y for now with TB data, will need to switch to eta, phi later
    int xdim = 5;
    int ydim = 5;
    std::vector<double> input_1D(NCHANNELS);
    std::vector<std::vector<double> > input_2D (xdim, std::vector<double> (ydim,0));
    std::vector<std::vector<double> > calibration_constants;
    std::vector<std::vector<double> > calibrated_2D;
    double ADC_GeV_conversion_factor = 0.0025;
    double threshold = 5000;
    double cluster_energy = 0;

    /*
    //temporarily fill 1D input with values
    for (int val=0; val<NCHANNELS;val ++){
        input_1D[val] = val ;
    }
    input_1D[12]=100;
    */
    
    // read in data from nano_scan_output
    std::string inFileName = "11393_nano_scan_output.txt";
    std::ifstream inFile;
    inFile.open(inFileName.c_str());
    std::vector<std::vector<double> > text_file_test_input(25, std::vector<double> (50,0));
    if (inFile.is_open())
    {
        
        std::cout << "Reading file " << inFileName << std::endl;
        
        for (int row = 0; row<50 ; row++) 
        {
            for (int crystal = 0; crystal < NCHANNELS; crystal++)
            {
                inFile >> text_file_test_input[crystal][row];
                //std::cout << text_file_test_input[crystal][row] << " ";
                    if ( ! inFile) {
                        std::cout << "Error reading file for crystal " << crystal << " row " << row << std::endl ; 
                        return 1;
                    }
            }
            std::cout << std::endl;
        }
        
        inFile.close(); // CLose input file
    }
    else { //Error message
        std::cerr << "Can't find input file " << inFileName << std::endl;
    }

    //temporarily take first line of file input and use it to fill 1D array
    for (int val=0; val<NCHANNELS;val ++){
        input_1D[val] = text_file_test_input[val][0];
    }

    // take 1D input and fill 2D array with crystal amplitude in correct positions
    for (int y = 0; y < input_2D.size(); y++) {
        for (int x = 0; x <input_2D[y].size(); x++) {
            input_2D[x][y] = input_1D[xdim-x-1+5*y];
        }
    }

    // print out 2D array
    std::cout << "The 2D input array is: \n";
      for (int a = 0; a < input_2D.size(); a++) { 
        for (int b = 0; b < input_2D[a].size(); b++) 
            std::cout << input_2D[a][b] << "\t"; 
        std::cout << std::endl; 
    }   

    /*
    std::cout << "Applying calibration constants and converting to GeV \n";
    // apply calibration constants to 2D array and convert to GeV
    for (int i=0; i<input_2D.size(); i++)
    {
        for (int j=0; j<input_2D[i].size(); j++)
        {
            calibrated_2D[i][j] = ADC_GeV_conversion_factor * calibration_constants[i][j] * input_2D[i][j];
        }
    }
    */
    std::cout << "Searching for crystals above threshold \n";
   
    // look to see how many crystals above threshold
    // create pointer to seed(s)
    int seeds = 0;
    for (int t = 0; t<NCHANNELS; t++){
        if (input_1D[t]>=threshold){
            seeds++;
        }
    }
    
    // if no crystals above threshold then finish
    if(seeds==0){
        std::cout << "There are no seeds at or above the threshold " << threshold << " GeV \n";
    }
    // if 1 crystal above threshold then cluster
    else if(seeds==1){
        std::cout << "There is one seed at or above the threshold " << threshold << " GeV \n";
        std::cout << "Clustering around seed ... \n";
        int seed_val = 0;
        int seed_x = 0;
        int seed_y = 0;
        for (int a = 0; a < input_2D.size(); a++) { 
            for (int b = 0; b < input_2D[a].size(); b++) {
                if (seed_val < input_2D[a][b]){
                    seed_val = input_2D[a][b];
                    seed_x = b;
                    seed_y = a;
                }
            }
        }   
        // check space around seed incase seed is at an edge
        if (seed_x == 0 || seed_x ==4 || seed_y == 0 || seed_y ==4) {
            std::cout << "Seed is at an edge or corner of crystal matrix. Cannot form cluster. \n";
        }
        else{
            for (int y_clus = seed_y-1; y_clus < seed_y+2; y_clus++){
            for (int x_clus = seed_x-1; x_clus < seed_x+2; x_clus++){
                cluster_energy+=input_2D[x_clus][y_clus];
                //std::cout << input_2D[x_clus][y_clus] << "\n";
            }
        }
        std::cout << "The seed has an energy of " << seed_val << " GeV and is located at " << seed_x << "," << seed_y << "\n";
        std::cout << "The cluster has an energy of " << cluster_energy << " GeV \n";
         }
    }

    // if 2 + crystals above threshold then ...
    else if(seeds>1){
        std::cout << "There are multiple seeds at or above the threshold " << threshold << " GeV \n";
    }
}